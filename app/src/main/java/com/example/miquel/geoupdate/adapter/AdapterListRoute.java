package com.example.miquel.geoupdate.adapter;

/**
 * Created by miquel on 30/12/15.
 */

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.miquel.geoupdate.R;

import com.example.miquel.geoupdate.RealmDAO.RouteDAO;
import com.example.miquel.geoupdate.activities.Route_FragmentActivity;
import com.example.miquel.geoupdate.model.Route;

import org.parceler.Parcels;

import java.text.DecimalFormat;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;


/**
 * Class that customises a listview to show the routes list
 *
 * @author Miquel Graells Monreal
 */

public class AdapterListRoute extends BaseAdapter implements OnClickListener {

    LayoutInflater layoutInflater;
    Activity activity;
    AdapterListRoute thisAdapter;
    RealmResults<Route> routesList;

    /**
     * @param activity which we will inflate the layout
     */
    public AdapterListRoute(Activity activity) {
        thisAdapter = this;
        layoutInflater = LayoutInflater.from(activity);
        this.activity = activity;
        loadRutesList();
    }

    @Override
    public int getCount() {
        return routesList.size();
    }

    @Override
    public Object getItem(int position) {
        return routesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    // Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
    // classe, per als objectes
    // que representen
    class ViewHolder {
        TextView nameList;
        TextView dateStart;
        TextView distance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            // Inflem el Layout de cada item
            convertView = layoutInflater.inflate(
                    R.layout.item, null);
            holder = new ViewHolder();
            // Capturem els TextView
            holder.nameList = (TextView) convertView
                    .findViewById(R.id.nameList);
            holder.dateStart = (TextView) convertView
                    .findViewById(R.id.startDate);
            holder.distance = (TextView) convertView
                    .findViewById(R.id.distance);
            // Associem el viewholder,
            // la informació de l'estructura que hi ha d'haver dins el layout,
            // amb la vista que haurà de retornar aquest mètode.
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        /*View item = convertView.findViewById(R.id.item);
        item.setTag(position);
        item.setOnClickListener(this);*/
        convertView.setTag(convertView.getId(),position);
        convertView.setOnClickListener(this);

        holder.nameList.setText(routesList.get(position).getName());
        holder.dateStart.setText(routesList.get(position).getDateStart());

        DecimalFormat df = new DecimalFormat("###.##");
        String distanceFormated = df.format(routesList.get(position).getDistance());
        holder.distance.setText(distanceFormated + " km");
        return convertView;
    }

    public void loadRutesList() {
        routesList = RouteDAO.findAll(activity);
        Log.d("sugardebug", String.valueOf(routesList.size()));
    }

    /**
     * metodo en el que gestionamos que passa aundo se clica en el nombre, el boton delete o el boton de play de cada lista
     */
    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag(v.getId());
        Route route = routesList.get(position);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(activity).build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Route r = realm.copyFromRealm(route);
        realm.commitTransaction();

        /*Intent intent = new Intent(activity, MapRoute.class);
        intent.putExtra("route", Parcels.wrap(r));
        activity.startActivity(intent);*/

        Intent intent = new Intent(activity, Route_FragmentActivity.class);
        intent.putExtra("route", Parcels.wrap(r));
        activity.startActivity(intent);


    }
}
package com.example.miquel.geoupdate.model;

import org.parceler.Parcel;

import io.realm.CoordRealmProxy;
import io.realm.RealmObject;

/**
 * Created by miquel on 26/12/15.
 *
 * Poi class that represents a coordinate.
 * Extends RealmObject to allow to be storage on local db
 * Implements Parcelable to allow pass object throw activities
 */

@Parcel(implementations = { CoordRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Coord.class })
public class Coord extends RealmObject {
    // lat coordinate
    double lat;
    // lng coordinate
    double lng;

    // obligate empty contructor to be saved on db using Realm
    public Coord(){

    }

    public Coord(double lat, double lng){
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Coord{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }

}


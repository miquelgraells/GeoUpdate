package com.example.miquel.geoupdate.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

/**
 * Created by miquel on 6/08/17.
 */

public class ServiceUtils {
    Activity activity;

    public ServiceUtils (Activity activity) {
        this.activity = activity;
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
package com.example.miquel.geoupdate.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.activities.DetailsActivity;
import com.example.miquel.geoupdate.model.Coord;
import com.example.miquel.geoupdate.model.ImageWithCoord;
import com.example.miquel.geoupdate.model.Route;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;

import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Map_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Map_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Map_Fragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    // object to holde the route
    static Route route = null;
    RealmList<ImageWithCoord> imagesWithCoord = null;
    View toogleMarkers = null;
    boolean isShowMarkers = true;
    ArrayList<Marker> markerList = new ArrayList<Marker>();

    private OnFragmentInteractionListener mListener;

    public Map_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Map_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Map_Fragment newInstance(Route route) {
        Map_Fragment fragment = new Map_Fragment();
        Map_Fragment.route = route;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map_, container, false);
        toogleMarkers = v.findViewById(R.id.togglemarkers);
        toogleMarkers.setOnClickListener(this);

        FragmentManager myFM = getChildFragmentManager();
        final SupportMapFragment mapFragment = (SupportMapFragment) myFM
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        return v;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap map) {

        // getting the list of coordines from route
        RealmList<Coord> arrCoord = route.getArrCoord();
        // gettin the first lat and lng to center the map
        double lat = arrCoord.get(0).getLat();
        double lng = arrCoord.get(0).getLng();

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14.0f));

        // show route on map
        PolylineOptions rectOptions = new PolylineOptions().color(Color.BLUE);
        for (int i = 0; i < arrCoord.size(); i++) {
            rectOptions.add(new LatLng(arrCoord.get(i).getLat(), arrCoord.get(i).getLng()));
        }
        map.addPolyline(rectOptions);

        LatLng endPoint = new LatLng(arrCoord.get(0).getLat(), arrCoord.get(0).getLng());
        map.addMarker(new MarkerOptions()
                .position(endPoint)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.start_point)));

        LatLng startPoint = new LatLng(arrCoord.get(arrCoord.size()-1).getLat(), arrCoord.get(arrCoord.size()-1).getLng());
        map.addMarker(new MarkerOptions()
                .position(startPoint)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.end_point)));

        //show points where pictures has been taken
        imagesWithCoord = route.getImagesWithCoord();
        for (int i = 0; i < imagesWithCoord.size(); i++) {
            double latImg = imagesWithCoord.get(i).getCoor().getLat();
            double lngImg = imagesWithCoord.get(i).getCoor().getLng();
            LatLng place = new LatLng(latImg, lngImg);

            MarkerOptions mo = new MarkerOptions();
            mo.position(place);
            mo.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.picture_on_map)));

            Marker m = map.addMarker(mo);
            m.setTag(String.valueOf(i));
            markerList.add(m);
        }

        map.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        int index = Integer.valueOf( (String) marker.getTag());
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra("imagePath", imagesWithCoord.get(index).getPath());
        startActivity(intent);

        return true;
    }

    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    @Override
    public void onClick(View v) {

        if (isShowMarkers) {
            v.setBackground(getResources().getDrawable(R.drawable.show_markers));
            isShowMarkers = false;
            for (int i = 0; i < markerList.size(); i++) {
                markerList.get(i).setVisible(false);
            }
        } else {
            v.setBackground(getResources().getDrawable(R.drawable.hide_markers));
            isShowMarkers = true;
            for (int i = 0; i < markerList.size(); i++) {
                markerList.get(i).setVisible(true);
            }
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

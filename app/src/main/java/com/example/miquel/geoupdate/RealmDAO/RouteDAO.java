package com.example.miquel.geoupdate.RealmDAO;

import android.content.Context;
import android.util.Log;

import com.example.miquel.geoupdate.model.Route;

import java.util.jar.Attributes;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by miquel on 8/08/16.
 * <p>
 * Class DAO to CRUD routes on realm local db
 */
public class RouteDAO {

    /**
     * inserts a route on db or updates in case that its already on db
     *
     * @param route   route object
     * @param context app context
     */
    public static void insert(Route route, Context context) {
        // initzializing realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        // getting the realm id from route to know if already its on db
        String realmId = route.getRealmId();
        RealmResults<Route> result2 = realm.where(Route.class)
                .equalTo("realmId", realmId)
                .findAll();


        // if route its on db it means that we want to update the customId
        if (result2.size() > 0 && result2.get(0) != null) {
            // gettin route in case that its on db
            Route routedb = result2.get(0);
            routedb.setCustomId(route.getCustomId());
            routedb.setOnWeb(route.getOnWeb());
            route = routedb;
        }
        Log.d("androiddebug", route.getRealmId());
        realm.copyToRealm(route);
        realm.commitTransaction();

    }

    /**
     * deletes route given on local db
     *
     * @param route   route object
     * @param context app context
     */
    public static void delete(Route route, Context context) {
        // initzializing realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        // getting the realm id from route to know if already its on db
        String realmId = route.getRealmId();
        RealmResults<Route> result2 = realm.where(Route.class)
                .equalTo("realmId", realmId)
                .findAll();
        // delete route object
        result2.deleteAllFromRealm();
        realm.commitTransaction();

    }

    /**
     * @param context app context
     * @return list of all routes on db
     */
    public static RealmResults<Route> findAll(Context context) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<Route> routes = realm.where(Route.class)
                .findAll();
        realm.commitTransaction();

        return routes;
    }

    public static int routeNumber(Context context) {
        // initzializing realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<Route> result2 = realm.where(Route.class)
                .findAll();
        realm.commitTransaction();


        return result2.size();
    }

    public static void updateRouteNumber(Context context, String realmID, String name) {
        // initzializing realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        // getting the realm id from route to know if already its on db
        RealmResults<Route> result = realm.where(Route.class)
                .equalTo("realmId", realmID)
                .findAll();


        // gettin route in case that its on db
        Route routedb = result.get(0);
        routedb.setName(name);
        realm.copyToRealm(routedb);

        realm.commitTransaction();
    }

}

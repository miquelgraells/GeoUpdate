package com.example.miquel.geoupdate.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.adapter.GridViewAdapter;
import java.util.ArrayList;

/**
 *  Activity that shows a grid of images
 */
public class Gallery extends Activity {
    // grid layout
    private GridView gridView;
    // adapter for grid layout
    private GridViewAdapter gridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        // gettin images paths from intent
        Intent i = getIntent();
        final ArrayList<String> imagePaths = i.getStringArrayListExtra("imagePaths");
        // inflating grid view and adapter
        gridView = (GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, imagePaths);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //intetn that will send the path of image picked to Details activity to be show
                Intent intent = new Intent(Gallery.this, DetailsActivity.class);
                intent.putExtra("imagePath", imagePaths.get(position));
                startActivity(intent);
            }
        });
    };

}

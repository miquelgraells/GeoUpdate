package com.example.miquel.geoupdate.activities;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.miquel.geoupdate.model.Coord;
import com.example.miquel.geoupdate.model.ImageWithCoord;
import com.example.miquel.geoupdate.model.Route;
import com.example.miquel.geoupdate.request.HttpAsyncTask;
import com.example.miquel.geoupdate.service.GeoService;
import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.utils.ServiceUtils;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import io.realm.RealmList;

//http://v4all123.blogspot.com.es/2013/12/using-db4o-in-android.html

/**
 * This activity is used to start and stop collecting coordinates, take pictures and one button that redirects to one activity
 * to see the gallery of pictures taken (Gallery class) and other button to redirect to other activity to see the route done on map (MapRoute)
 */
public class RecordCoor extends AppCompatActivity implements android.view.View.OnClickListener{

    // buttons states of start and stop collecting coordinates for enabling or disabling other buttons
    static boolean START_UPDATES_BUTTON_STATE = true;
    // code in case a picthre is taken
    static final int REQUEST_IMAGE_CAPTURE = 1;
    // paths images taken to sen to gallery
    public static ArrayList<String> imagePaths = new ArrayList<String>();
    // images with coordines list
    public static RealmList<ImageWithCoord> imagesWithCoord = new RealmList<ImageWithCoord> ();
    // activitie's buttons
    protected ImageView mStartUpdatesButton;
    protected ImageView mStopUpdatesButton;
    protected ImageButton takePictureButton;
    protected ImageButton galleryButton;
    protected ImageButton seeMapButton;
    View startStopBtnsStart;
    View startStopBtnsStop;
    View cameraPhotosMapEnabled;
    View cameraPhotosMapDisabled;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Locate the UI widgets.
        startStopBtnsStart = findViewById(R.id.startBtns);
        startStopBtnsStop = findViewById(R.id.stopBtns);

        cameraPhotosMapEnabled = findViewById(R.id.camera_photos_map_enabled);
        cameraPhotosMapDisabled = findViewById(R.id.camera_photos_map_disabled);

        mStartUpdatesButton = (ImageView) findViewById(R.id.start_updates_button);
        mStartUpdatesButton.setOnClickListener(this);

        mStopUpdatesButton = (ImageView) findViewById(R.id.stop_updates_button);
        mStopUpdatesButton.setOnClickListener(this);

        takePictureButton = (ImageButton) findViewById(R.id.take_picture_button);
        galleryButton = (ImageButton) findViewById(R.id.gallery_button);
        seeMapButton = (ImageButton) findViewById(R.id.see_map_button);

        takePictureButton.setOnClickListener(this);
        galleryButton.setOnClickListener(this);
        seeMapButton.setOnClickListener(this);

        ServiceUtils serviceUtils = new ServiceUtils(this);
        boolean isServiceRuning = serviceUtils.isMyServiceRunning(GeoService.class);
        if(isServiceRuning){
            hideStartRouteButtons();
            showCameraPhotosMapButtons();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    // pick the picture taken
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == RESULT_OK) {

                    String[] projection = {MediaStore.Images.Media.DATA };
                    Cursor cursor = managedQuery(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            projection, null, null, null);
                    int column_index_data = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();

                    String imagePath = cursor.getString(column_index_data);
                    imagePaths.add(imagePath);
                    //get coordines for image taken and addin to list
                    Coord coord = GeoService.route.getArrCoord().get(GeoService.route.getArrCoord().size()-1);
                    imagesWithCoord.add(new ImageWithCoord(imagePath,coord));

                }
                break;
            default :
                Log.d("androidDebug","default");
        }
    }

    @Override
    public void onClick(View v) {
        if(v == mStartUpdatesButton){
            showCameraPhotosMapButtons();
            hideStartRouteButtons();
            startService(new Intent(getBaseContext(), GeoService.class));
        } else if(v == mStopUpdatesButton) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(getString(R.string.stop_route));
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    stopService(new Intent(getBaseContext(), GeoService.class));
                                    finish();
                                }
                            })
                    .setNegativeButton(getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
            // create an alert dialog
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        } else if (v == takePictureButton) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        } else if (v == galleryButton) {
            Intent intent = new Intent(this, Gallery.class);
            intent.putStringArrayListExtra("imagePaths", imagePaths);
            startActivity(intent);
        } else if (v == seeMapButton) {
            // MapRoute will get the route from GeoService because the route its not yet saved on the DB
            //Intent intent = new Intent(this, MapRoute.class);
            Intent intent = new Intent(this, MapRouteActivity.class);
            this.startActivity(intent);
        }
    }

    /*
     * hides and disable the start route buttons and shows and enables the stop route buttons
     */
    private void hideStartRouteButtons(){
        startStopBtnsStart.setVisibility(View.GONE);
        startStopBtnsStart.setEnabled(false);
        startStopBtnsStop.setVisibility(View.VISIBLE);
        startStopBtnsStop.setEnabled(true);
    }

    /*
    * shows and enable the CameraPhotosMap
    */
    private void showCameraPhotosMapButtons(){
        cameraPhotosMapEnabled.setVisibility(View.VISIBLE);
        cameraPhotosMapEnabled.setEnabled(true);
        cameraPhotosMapDisabled.setVisibility(View.GONE);
        cameraPhotosMapDisabled.setEnabled(false);
    }
}
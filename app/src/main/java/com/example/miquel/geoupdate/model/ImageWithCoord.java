package com.example.miquel.geoupdate.model;


import org.parceler.Parcel;

import io.realm.ImageWithCoordRealmProxy;
import io.realm.RealmObject;

/**
 * Created by miquel on 14/07/16.
 *
 * Poi class that represents an image with the coordinets where was taken.
 * Extends RealmObject to allow to be storage on local db
 * Implements Parcelable to allow pass object throw activitie
 *
 */

@Parcel(implementations = { ImageWithCoordRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { ImageWithCoord.class })
public class ImageWithCoord extends RealmObject {

    // image path
    String path;
    // image coordinates
    Coord coor;
    // string image coded 64. Not saved on local db. just used to send to server
    private String codeImage;

    // obligate empty contructor to be saved on db using Realm
    public ImageWithCoord() {

    }

    public ImageWithCoord(String path, Coord coor) {
        this.path = path;
        this.coor = coor;
    }

    public Coord getCoor() {
        return coor;
    }

    public void setCoor(Coord coor) {
        this.coor = coor;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCodeImage() {
        return codeImage;
    }

    public void setCodeImage(String codeImage) {
        this.codeImage = codeImage;
    }

}


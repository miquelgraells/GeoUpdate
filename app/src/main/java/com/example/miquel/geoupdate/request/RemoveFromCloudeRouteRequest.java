package com.example.miquel.geoupdate.request;


import com.example.miquel.geoupdate.enviroment.Enviroment;
import com.example.miquel.geoupdate.model.Route;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class RemoveFromCloudeRouteRequest {

    private static AsyncHttpClient client = new AsyncHttpClient();
    private ArrayList<RemoveFromCloudeRouteRequest.OnResponse> onResponses = new ArrayList<>();
    private String url = Enviroment.enviroment + "/api/removeRouteById";
    RequestParams params ;

    public RemoveFromCloudeRouteRequest (String routeID) {
        params = new RequestParams();
        params.put("idRoute", routeID);
    }

    public void delete () {

        client.delete(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    int status = response.getInt("status");
                    if (status == 200) {
                        for (int i = 0; i < onResponses.size(); i++) {
                            onResponses.get(i).onRemoveFormCloudSucced(response);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                for (int i = 0; i < onResponses.size(); i++) {
                    onResponses.get(i).onRemoveFormCloudFilure();
                }
            }
        });
    }

    public interface OnResponse {
        void onRemoveFormCloudSucced(JSONObject response);

        void onRemoveFormCloudFilure();
    }

    public void addOnOnResponse(RemoveFromCloudeRouteRequest.OnResponse onResponse) {
        onResponses.add(onResponse);
    }
}

package com.example.miquel.geoupdate.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


import com.example.miquel.geoupdate.RealmDAO.RouteDAO;
import com.example.miquel.geoupdate.activities.RecordCoor;
import com.example.miquel.geoupdate.model.Coord;
import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.model.Route;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;


/**
 * Created by miquel on 29/12/15.
 * 
 * service that gets actuall coordinates. When destroyed saves the coordinates on local db
 */

    public class GeoService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "location-updates-sample";

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */
    protected Location mCurrentLocation;

    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;

    /**
     * Stores the hole route
     */
    public static Route route;

        /** Called when the service is being created. */
        @Override
        public void onCreate() {
            route = new Route();
            // set custom id to hadle with realm
            route.setRealmId(String.valueOf(hashCode()));

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Calendar cal = Calendar.getInstance();
            String dateStart = dateFormat.format(cal.getTime());

            route.setDateStart(dateStart);

            Log.d("geoupdateLog",dateStart);

            mLastUpdateTime = "";
            buildGoogleApiClient();
        }

        @Override
        public IBinder onBind(Intent arg0) {
            return null;
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            mGoogleApiClient.connect();
            //startLocationUpdates();
            return START_STICKY;
        }

        @Override
        public void onDestroy() {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Calendar cal = Calendar.getInstance();
            String dateEnd = dateFormat.format(cal.getTime());
            route.setDateEnd(dateEnd);

            int number = RouteDAO.routeNumber(getApplicationContext());
            route.setName("Route " + number);

            route.setImagesWithCoord(RecordCoor.imagesWithCoord);

            // calculating route distance
            float totalDistance = 0;
            for (int i = 0; i < route.getArrCoord().size() - 1; i++) {
                Location currLocation = new Location("this");
                currLocation.setLatitude(route.getArrCoord().get(i).getLat());
                currLocation.setLongitude(route.getArrCoord().get(i).getLng());

                Location lastLocation = new Location("this");
                lastLocation.setLatitude(route.getArrCoord().get(i+1).getLat());
                lastLocation.setLongitude(route.getArrCoord().get(i+1).getLng());

                totalDistance += lastLocation.distanceTo(currLocation);
            }
            double km = totalDistance/1000;
            route.setDistance(km);

            // save route on db
            RouteDAO.insert(route, getApplicationContext());
            super.onDestroy();
        }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        saveCoord();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
        try{
            startLocationUpdates();
        } catch (NullPointerException e){
            Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            gpsOptionsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(gpsOptionsIntent);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        saveCoord();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * add actuall coord to route
     */
    private void saveCoord() {
        Coord c = new Coord(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        route.addCoord(c);
    }

}


package com.example.miquel.geoupdate.activities;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.adapter.AdapterListRoute;
import com.example.miquel.geoupdate.service.GeoService;
import com.example.miquel.geoupdate.utils.ServiceUtils;

public class ListRoutes_Activity extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton fab;
    FloatingActionButton fabDoingRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_routes_);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
        fabDoingRoute = (FloatingActionButton) findViewById(R.id.fabDoingRoute);
        fabDoingRoute.setOnClickListener(this);

        setFabIconState();
        loadRutesList();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, RecordCoor.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    protected void onRestart() {
        loadRutesList();
        setFabIconState();
        super.onRestart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.aboutit) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
     *  inflates a list and sets adapter
     */
    public void loadRutesList(){
        AdapterListRoute adapter = new AdapterListRoute(this);
        ListView lv = (ListView) findViewById(R.id.mylist);
        lv.setAdapter(adapter);
    }

    public void setFabIconState() {
        ServiceUtils serviceUtils = new ServiceUtils(this);
        boolean isGeoserviceRunning = serviceUtils.isMyServiceRunning(GeoService.class);
        if (isGeoserviceRunning) {
            fab.setVisibility(View.GONE);
            fabDoingRoute.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
            fabDoingRoute.setVisibility(View.GONE);
        }
    }
}

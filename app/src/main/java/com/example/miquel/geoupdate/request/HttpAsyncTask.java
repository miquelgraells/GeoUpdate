package com.example.miquel.geoupdate.request;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.example.miquel.geoupdate.model.Coord;
import com.example.miquel.geoupdate.model.ImageWithCoord;
import com.example.miquel.geoupdate.model.Route;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by miquel on 13/02/16.
 *
 * Class that handles a CRUD route to server
 *
 */
public class HttpAsyncTask extends AsyncTask<String, Void, String> {

    // route to be uploaded or removed to server
    private Route route;

    public HttpAsyncTask(){
    }

    public HttpAsyncTask(Route route){
        this.route = route;
    }

    @Override
    protected String doInBackground(String... urls) {
        // checking url given
        // url to upload
        if(urls[0].equals("http://nodejs-mgraells.rhcloud.com/insertroute")){
            // array of images with coordinates to be upload
            ImageWithCoord[] images = new ImageWithCoord[route.getImagesWithCoord().size()];
            for(int i = 0; i <  route.getImagesWithCoord().size(); i++){
                String path = route.getImagesWithCoord().get(i).getPath();
                Coord coord = route.getImagesWithCoord().get(i).getCoor();
                ImageWithCoord imageWithCoord = new ImageWithCoord(path,coord);
                // seting encode image 64 to up to server
                imageWithCoord.setCodeImage(getStringImage(decodeFile(new File(route.getImagesWithCoord().get(i).getPath()))));
                images[i] = imageWithCoord;
            }
            // passing the route and array of images with image encoded to 64
            return insertRouteToServer(urls[0], route, images);

        } else if (urls[0].equals("http://nodejs-mgraells.rhcloud.com/removeRouteById")) { // url to remove
          //  return removeRouteToServer(urls[0], route);
        }
        return "";
    }
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {

    }

    /**
     * uploads route to server
     * @param url endpoint to upload route
     * @param route route to upload
     * @param images images to upload
     * @return the customId for route
     */
    public static String insertRouteToServer(String url, Route route, ImageWithCoord[] images){
        String id = "";
        int res = 0;
        URL object= null;
        try {
            object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            JSONArray jsonArrayImatges = new JSONArray();
            for(int i = 0; i < images.length; i++){
                JSONObject o = new JSONObject();
                Log.d("nodejs", "base64 : " + images[i].getCodeImage());
                o.put("image", images[i].getCodeImage());
                o.put("lat", images[i].getCoor().getLat());
                o.put("lng", images[i].getCoor().getLng());
                jsonArrayImatges.put(o);
            }

            JSONArray jsonArrayRoute = new JSONArray();
            Log.d("httpHash", String.valueOf(route.hashCode()));

            for(int i = 0; i<route.getArrCoord().size();i++){
                JSONObject jsonCoor = new JSONObject();
                Coord c = route.getArrCoord().get(i);
                double lat = c.getLat();
                double lng = c.getLng();
                jsonCoor.put("lat", lat);
                jsonCoor.put("lng",lng);

                jsonArrayRoute.put(jsonCoor);
            }

            JSONObject jsonObjectToSend = new JSONObject();
            jsonObjectToSend.put("images", jsonArrayImatges);
            jsonObjectToSend.put("idRoute", route.hashCode());
            jsonObjectToSend.put("coor", jsonArrayRoute);

            con.setFixedLengthStreamingMode(jsonObjectToSend.toString().getBytes().length);
            con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(jsonObjectToSend.toString());
            wr.flush();
            wr.close();


            try {
                //Receive the response from the server
                InputStream in = new BufferedInputStream(con.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                Log.d("JSON androiddebug", "result: " + result.toString());

                JSONObject jObj = new JSONObject(result.toString());
                id = jObj.getString("id");
                Log.d("JSON androiddebug", "id: " + id);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

            res = con.getResponseCode();
            con.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e ){
            e.printStackTrace();
        } catch (JSONException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("androiddebug",String.valueOf(res));

        return id;

    }
/*
    /**
     *
     * @param url endpoint to remove route
     * @param route route to be removed
     * @return
     */

  /*  public static String removeRouteToServer(String url, Route route){
        Log.d("JSON androiddebug", "remove id : first");
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("idRoute", route.getCustomId());
            Log.d("JSON androiddebug", "remove id : " + String.valueOf(route.getCustomId()));
            Log.d("JSON androiddebug", "remove id : " + String.valueOf(route.getOnWeb()));

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if(inputStream != null) {
                result = convertInputStreamToString(inputStream);
            }
            else {
                result = "Did not work!";
            }

        } catch (Exception e) {
            e.printStackTrace();
            //Log.d("InputStream", e.getLocalizedMessage());
        }

        // 11. return result
        return result;
    }*/

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    /**
     *
     * @param bmp bitmap image
     * @return base 64 string
     */
    public static String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    /**
     * return a bitmap for a given image path
     * @param f file path
     * @return
     */
    public static Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE=300;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
}
package com.example.miquel.geoupdate.model;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RouteRealmProxy;

/**
 * Created by miquel on 26/12/15.
 *
 * Poi class that represents a route
 * Extends RealmObject to allow to be storage on local db
 * Implements Parcelable to allow pass object throw activitie
 *
 */

@Parcel(implementations = { RouteRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Route.class })
public class Route extends RealmObject {

    // to know if route is on web or just on app
    Boolean isOnWeb;
    // list of route coordinates
    RealmList<Coord> arrCoord = new RealmList<Coord>();
    // list of images with coordinates
    RealmList<ImageWithCoord> imagesWithCoord = new RealmList<ImageWithCoord>();
    // id given when route is up to web
    String customId;
    // id when route is saved on local db
    String realmId;
    // route name
    String name;
    // route date started dd/mm/yy hh:mm
    String dateStart;
    // route date ened dd/mm/yy hh:mm
    String dateEnd;
    // route distance km
    double distance;

    // obligate empty contructor to be saved on db using Realm
    public Route(){
    }

    public void addCoord(Coord c){
        arrCoord.add(c);
    }

    public Boolean getOnWeb() {
        return isOnWeb;
    }

    public void setOnWeb(Boolean onWeb) {
        isOnWeb = onWeb;
    }

    @ParcelPropertyConverter(RealmListParcelConverter.class)
    public RealmList<ImageWithCoord> getImagesWithCoord() {
        return imagesWithCoord;
    }

    public void setImagesWithCoord(RealmList<ImageWithCoord> imagesWithCoord) {
        this.imagesWithCoord = imagesWithCoord;
    }

    public RealmList<Coord> getArrCoord() {
        return arrCoord;
    }

    @ParcelPropertyConverter(RealmListParcelConverter.class)
    public void setArrCoord(RealmList<Coord> arrCoord) {
        this.arrCoord = arrCoord;
    }

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public String getRealmId() {
        return realmId;
    }

    public void setRealmId(String realmId) {
        this.realmId = realmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Route{" +
                "isOnWeb=" + isOnWeb +
                ", arrCoord=" + arrCoord +
                ", imagesWithCoord=" + imagesWithCoord +
                ", customId='" + customId + '\'' +
                ", realmId='" + realmId + '\'' +
                '}';
    }


}

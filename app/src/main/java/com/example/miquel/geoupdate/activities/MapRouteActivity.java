package com.example.miquel.geoupdate.activities;

import android.graphics.Color;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.model.Coord;
import com.example.miquel.geoupdate.model.Route;
import com.example.miquel.geoupdate.service.GeoService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.parceler.Parcels;

import java.text.DecimalFormat;

import io.realm.RealmList;

public class MapRouteActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;
    RealmList<Coord> arrCoord;
    TextView kmTv;
    public Route route = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_route);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        kmTv = (TextView) findViewById(R.id.kmTv);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.maproute);
        mapFragment.getMapAsync(this);

        route = GeoService.route;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        arrCoord = route.getArrCoord();
        double lat = arrCoord.get(0).getLat();
        double lng = arrCoord.get(arrCoord.size()-1).getLng();

        map.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng) , 14.0f) );

        float totalDistance = 0;
        for (int i = 0; i < route.getArrCoord().size() - 1; i++) {
            Location currLocation = new Location("this");
            currLocation.setLatitude(route.getArrCoord().get(i).getLat());
            currLocation.setLongitude(route.getArrCoord().get(i).getLng());

            Location lastLocation = new Location("this");
            lastLocation.setLatitude(route.getArrCoord().get(i+1).getLat());
            lastLocation.setLongitude(route.getArrCoord().get(i+1).getLng());

            totalDistance += lastLocation.distanceTo(currLocation);
        }
        double km = totalDistance/1000;
        DecimalFormat df = new DecimalFormat("###.##");
        String distance = df.format(km);
        kmTv.setText(distance + " km");

        PolylineOptions rectOptions = new PolylineOptions().color(Color.BLUE);
        for (int i = 0; i < arrCoord.size(); i++) {
            rectOptions.add(new LatLng(arrCoord.get(i).getLat(), arrCoord.get(i).getLng()));
        }
        map.addPolyline(rectOptions);

        LatLng endPoint = new LatLng(arrCoord.get(0).getLat(), arrCoord.get(0).getLng());
        googleMap.addMarker(new MarkerOptions()
                .position(endPoint)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.start_point)));

        LatLng startPoint = new LatLng(arrCoord.get(arrCoord.size()-1).getLat(), arrCoord.get(arrCoord.size()-1).getLng());
        googleMap.addMarker(new MarkerOptions()
                .position(startPoint)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.end_point)));
    }
}

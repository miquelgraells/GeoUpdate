package com.example.miquel.geoupdate.activities;


import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.fragments.Gallery_Fragment;
import com.example.miquel.geoupdate.fragments.Map_Fragment;
import com.example.miquel.geoupdate.fragments.RouteSettings_Fragment;
import com.example.miquel.geoupdate.model.ImageWithCoord;
import com.example.miquel.geoupdate.model.Route;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class Route_FragmentActivity extends AppCompatActivity implements Map_Fragment.OnFragmentInteractionListener, Gallery_Fragment.OnFragmentInteractionListener, RouteSettings_Fragment.OnFragmentInteractionListener,BottomNavigationView.OnNavigationItemSelectedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    Route route = null;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route__fragment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        route = Parcels.unwrap(getIntent().getParcelableExtra("route"));

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        Map_Fragment map_fragment = Map_Fragment.newInstance(route);
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.flContent, map_fragment);
        ft.commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_map:
                Map_Fragment map_fragment = Map_Fragment.newInstance(route);
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flContent, map_fragment);
                ft.commit();
                break;
            case R.id.action_gallery:
                ArrayList<String> imagePaths = new ArrayList<>();
                for(int i = 0; i < route.getImagesWithCoord().size(); i++){
                    imagePaths.add(route.getImagesWithCoord().get(i).getPath());
                }

                Gallery_Fragment gallery_fragment = Gallery_Fragment.newInstance(imagePaths);
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flContent, gallery_fragment);
                ft.commit();
                break;
            case R.id.action_statistics:

                break;
            case R.id.action_settings:
                RouteSettings_Fragment routeSettings_fragment = RouteSettings_Fragment.newInstance(route);
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flContent, routeSettings_fragment);
                ft.commit();

                break;
        }
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

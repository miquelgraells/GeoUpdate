package com.example.miquel.geoupdate.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.RealmDAO.RouteDAO;
import com.example.miquel.geoupdate.model.Route;
import com.example.miquel.geoupdate.request.RemoveFromCloudeRouteRequest;
import com.example.miquel.geoupdate.request.UploadRoute_Request;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RouteSettings_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RouteSettings_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RouteSettings_Fragment extends Fragment implements View.OnClickListener, UploadRoute_Request.OnResponse, RemoveFromCloudeRouteRequest.OnResponse {


    private OnFragmentInteractionListener mListener;
    static Route route;
    View uploadBtn;
    View removeBtn;
    View removeFomCloudBtn;
    private View mProgressView;
    AVLoadingIndicatorView progress;
    EditText routeName;

    // Required empty public constructor
    public RouteSettings_Fragment() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RouteSettings_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RouteSettings_Fragment newInstance(Route route) {
        RouteSettings_Fragment fragment = new RouteSettings_Fragment();
        RouteSettings_Fragment.route = route;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_route_settings_, container, false);
        uploadBtn = v.findViewById(R.id.upload);
        uploadBtn.setOnClickListener(this);

        removeBtn = v.findViewById(R.id.remove);
        removeBtn.setOnClickListener(this);

        removeFomCloudBtn = v.findViewById(R.id.removeFromCloud);
        removeFomCloudBtn.setOnClickListener(this);

        mProgressView = v.findViewById(R.id.login_progress);
        mProgressView.bringToFront();
        //progress = (AVLoadingIndicatorView) v.findViewById(R.id.avi);

        routeName = (EditText) v.findViewById(R.id.routeName);
        routeName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (routeName.getRight() - routeName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        RouteDAO.updateRouteNumber(getActivity(),route.getRealmId(),routeName.getText().toString());

                        Toast toast = Toast.makeText(getActivity(), "name updated", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
                return false;
            }
        });

        routeName.setText(route.getName());

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == uploadBtn) {
            mProgressView.setVisibility(View.VISIBLE);

            UploadRoute_Request uploadRoute_request = new UploadRoute_Request(route);
            uploadRoute_request.addOnOnResponse(this);
            uploadRoute_request.post();
        } else if (v == removeBtn) {
            try {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                alertDialogBuilder.setTitle("Remove from device ?");
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Log.d("routeDebug", "2 : " + route.toString());
                                        Log.d("routeDebug", "2 : " + route.hashCode());
                                        deleteRoute();
                                        getActivity().finish();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });
                // create an alert dialog
                AlertDialog alert = alertDialogBuilder.create();
                alert.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (v == removeFomCloudBtn) {
            mProgressView.setVisibility(View.VISIBLE);
            RemoveFromCloudeRouteRequest removeFromCloudeRouteRequest = new RemoveFromCloudeRouteRequest(route.getCustomId());
            removeFromCloudeRouteRequest.addOnOnResponse(this);
            removeFromCloudeRouteRequest.delete();
        }
    }

    //-------------UploadRoute_Request listener--------------------
    @Override
    public void onUploadSucced(JSONObject response) {
        mProgressView.setVisibility(View.GONE);

        CharSequence text = "Upload succed";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(getActivity(), text, duration);
        toast.show();

        try {
            String id = response.getString("id");
            updateRouteId(id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUploadFail() {
        mProgressView.setVisibility(View.GONE);

        CharSequence text = "Something went wrong";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(getActivity(), text, duration);
        toast.show();
    }

    @Override
    public void onFilure() {
        mProgressView.setVisibility(View.GONE);

        CharSequence text = "Fail connection";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(getActivity(), text, duration);
        toast.show();
    }
    //-------------UploadRoute_Request listener--------------------


    //-------- ----RemoveFromCloudRouteRequest Listener -----------
    @Override
    public void onRemoveFormCloudSucced(JSONObject response) {
        mProgressView.setVisibility(View.GONE);
        CharSequence text = "Route deleted from web";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(getActivity(), text, duration);
        toast.show();
    }

    @Override
    public void onRemoveFormCloudFilure() {
        mProgressView.setVisibility(View.GONE);
        CharSequence text = "Something went wrong";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(getActivity(), text, duration);
        toast.show();
    }
    //-------------RemoveFromCloudRouteRequest Listener -----------


    /**
     * delete route on local db
     */
    private void deleteRoute(){
        RouteDAO.delete(route,getActivity());
    }

    /**
     * saving customId on route and updating to true on web state
     * @param customId
     */
    private void updateRouteId(String customId){
        route.setCustomId(customId);
        route.setOnWeb(true);
        RouteDAO.insert(route,getActivity());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}

package com.example.miquel.geoupdate.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.miquel.geoupdate.R;
import com.example.miquel.geoupdate.activities.DetailsActivity;
import com.example.miquel.geoupdate.activities.Gallery;
import com.example.miquel.geoupdate.adapter.GridViewAdapter;

import java.util.ArrayList;
import java.util.concurrent.RecursiveTask;

import static com.example.miquel.geoupdate.R.id.gridView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Gallery_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Gallery_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Gallery_Fragment extends Fragment {
    static ArrayList<String> imagePaths;

    private OnFragmentInteractionListener mListener;

    public Gallery_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Gallery_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Gallery_Fragment newInstance (ArrayList<String> imagePaths) {
        Gallery_Fragment fragment = new Gallery_Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        Gallery_Fragment.imagePaths = imagePaths;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_gallery_, container, false);

        GridView gridView = (GridView) v.findViewById(R.id.gridView);
        GridViewAdapter gridAdapter = new GridViewAdapter(getActivity(), R.layout.grid_item_layout, Gallery_Fragment.imagePaths);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //intetn that will send the path of image picked to Details activity to be show
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra("imagePath", Gallery_Fragment.imagePaths.get(position));
                startActivity(intent);
            }
        });

        return  v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

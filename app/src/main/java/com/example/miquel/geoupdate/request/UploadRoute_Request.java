package com.example.miquel.geoupdate.request;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.example.miquel.geoupdate.enviroment.Enviroment;
import com.example.miquel.geoupdate.model.Coord;
import com.example.miquel.geoupdate.model.Route;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by miquel on 7/03/17.
 */

public class UploadRoute_Request {
    private static AsyncHttpClient client = new AsyncHttpClient();
    private ArrayList<UploadRoute_Request.OnResponse> onResponses = new ArrayList<>();
    private String url = Enviroment.enviroment + "/api/insertroute";
    RequestParams params ;
    Route route;

    public UploadRoute_Request (Route route) {
        params = new RequestParams();
        this.route = route;
    }

    public void prepareJSONData() {
        try {

            JSONArray jsonArrayImatges = new JSONArray();
            for (int i = 0; i < route.getImagesWithCoord().size(); i++) {
                JSONObject o = new JSONObject();
                String imageCode = getStringImage(decodeFile(new File(route.getImagesWithCoord().get(i).getPath())));
                Log.d("nodejs", "base64 : " + imageCode);
                //route.getImagesWithCoord().get(i).setCodeImage(imageCode);
                o.put("image", imageCode);
                o.put("lat", route.getImagesWithCoord().get(i).getCoor().getLat());
                o.put("lng", route.getImagesWithCoord().get(i).getCoor().getLng());
                jsonArrayImatges.put(o);
            }

            JSONArray jsonArrayRoute = new JSONArray();
            Log.d("httpHash", String.valueOf(route.hashCode()));
            for(int i = 0; i<route.getArrCoord().size();i++){
                JSONObject jsonCoor = new JSONObject();
                Coord c = route.getArrCoord().get(i);
                double lat = c.getLat();
                double lng = c.getLng();
                jsonCoor.put("lat", lat);
                jsonCoor.put("lng",lng);

                jsonArrayRoute.put(jsonCoor);
            }

            if(jsonArrayImatges.length() > 0) {
                params.put("images",jsonArrayImatges);
            }
            params.put("coor", jsonArrayRoute);
            JSONObject startRouteO = new JSONObject();
            startRouteO.put("lng",route.getArrCoord().get(0).getLng());
            startRouteO.put("lat",route.getArrCoord().get(0).getLat());
            params.put("startRoute", startRouteO);
            JSONObject statisticsO = new JSONObject();
            statisticsO.put("dateStart",route.getDateStart());
            statisticsO.put("dateEnd",route.getDateEnd());
            statisticsO.put("distance",route.getDistance());
            params.put("statistics", statisticsO);
            params.put("name", route.getName());


        } catch (JSONException e) {

        }
    }

    public void post() {
        prepareJSONData();
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    int status = response.getInt("status");
                    if (status == 200) {
                        for (int i = 0; i < onResponses.size(); i++) {
                            onResponses.get(i).onUploadSucced(response);
                        }
                    } else if (status == 301) {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                for (int i = 0; i < onResponses.size(); i++) {
                    onResponses.get(i).onFilure();
                }
            }
        });
    }

    public interface OnResponse {
        void onUploadSucced(JSONObject response);

        void onUploadFail();

        void onFilure();
    }

    public void addOnOnResponse(UploadRoute_Request.OnResponse onResponse) {
        onResponses.add(onResponse);
    }

    /**
     *
     * @param bmp bitmap image
     * @return base 64 string
     */
    public static String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    /**
     * return a bitmap for a given image path
     * @param f file path
     * @return
     */
    public static Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE=300;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

}
